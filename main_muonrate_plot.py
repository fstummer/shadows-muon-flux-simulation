import pybdsim
import pymadx
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.colors as colors
from matplotlib import path
from matplotlib.widgets import Slider, Button, RadioButtons, TextBox, RangeSlider, Cursor
import math
import sys
import re


def myround(x, base=5):
    return np.around(base * np.around(x/base, decimals=0), 5)

def myfloor(x, base=5):
    return base * math.floor(x/base)

def myceil(x, base=5):
    return base * math.ceil(x/base)

def equal_float(a, b):
    return abs(a - b) <= 1e-5

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]


class tile:
    def __init__(self, data, zloc, xmin, xmax, ymin, ymax, partperjob, oversampling, POTperSpill, spillDuration, particleType='13', pTypeShown='muAll'):
        self.data = data
        self.zloc = zloc
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax
        self.partperjob = partperjob
        self.oversampling = oversampling
        self.POTperSpill = POTperSpill
        self.spillDuration = spillDuration
        self.particleType = particleType
        self.pTypeShown = pTypeShown
        self.coordinates = [[self.xmin, self.ymin], [self.xmin, self.ymax], [self.xmax, self.ymax], [self.xmax, self.ymin]]
        self.hm = self.data.histograms2dpy["Event/SimpleHistograms/z{}mL_XY_+{}".format(str(int(self.zloc)), self.particleType)]
        self.hp = self.data.histograms2dpy["Event/SimpleHistograms/z{}mL_XY_-{}".format(str(int(self.zloc)), self.particleType)]
        self.minBinSizeX = np.abs(round(self.hm.xedges[0]-self.hm.xedges[1], 5))
        self.minBinSizeY = np.abs(round(self.hm.yedges[0]-self.hm.yedges[1], 5))
        self.sizeX = np.abs(round(self.xmax - self.xmin, 5))
        self.sizeY = np.abs(round(self.ymax - self.ymin, 5))
        assert round(self.sizeX / self.minBinSizeX, 5).is_integer(), "Limit of the histogram binning. {} is not a multiple of {}.".format(self.sizeX, self.minBinSizeX)
        assert round(self.sizeY / self.minBinSizeY, 5).is_integer(), "Limit of the histogram binning. {} is not a multiple of {}.".format(self.sizeY, self.minBinSizeY)
        self.area = self.sizeX * self.sizeY
        if(self.oversampling==1):
            # muons simulated directly
            self.POTsimulated = self.data.header.nOriginalEvents
        else:
            # muons simulated effectively using biasing techniques
            self.POTsimulated = len(self.data.header.combinedFiles) * self.partperjob * self.oversampling
        self.UpdateMuonrate()
        self.UpdateParticleTypeShown(self.pTypeShown)

    def UpdateTileSize(self, xmin, xmax, ymin, ymax):
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax
        self.coordinates = [[self.xmin, self.ymin], [self.xmin, self.ymax], [self.xmax, self.ymax], [self.xmax, self.ymin]]
        self.sizeX = np.abs(round(self.xmax - self.xmin, 5))
        self.sizeY = np.abs(round(self.ymax - self.ymin, 5))
        assert round(self.sizeX / self.minBinSizeX, 5).is_integer(), "Limit of the histogram binning. {} is not a multiple of {}.".format(self.sizeX, self.minBinSizeX)
        assert round(self.sizeY / self.minBinSizeY, 5).is_integer(), "Limit of the histogram binning. {} is not a multiple of {}.".format(self.sizeY, self.minBinSizeY)
        self.area = self.sizeX * self.sizeY
        self.UpdateMuonrate()

    def GetNumMuons(self, h):
        xlowerlim = next(x for x, val in enumerate(h.xcentres) if val>=self.xmin)
        xupperlim = next(x for x, val in enumerate(h.xcentres) if val>self.xmax)
        ylowerlim = next(y for y, val in enumerate(h.ycentres) if val>=self.ymin)
        yupperlim = next(y for y, val in enumerate(h.ycentres) if val>self.ymax)
        n = sum(sum(h.contents[xlowerlim: xupperlim, ylowerlim: yupperlim]))
        return n

    def UpdateZLocation(self, znew):
        self.zloc = znew
        self.hm = self.data.histograms2dpy["Event/SimpleHistograms/z{}mL_XY_+{}".format(str(int(self.zloc)), self.particleType)]
        self.hp = self.data.histograms2dpy["Event/SimpleHistograms/z{}mL_XY_-{}".format(str(int(self.zloc)), self.particleType)]
        self.UpdateMuonrate()

    def UpdateMuonrate(self):
        try:
            self.numMuMinus = self.GetNumMuons(self.hm)
        except:
            self.numMuMinus = 0
        try:
            self.numMuPlus  = self.GetNumMuons(self.hp)
        except:
            self.numMuPlus = 0
        self.rateMuMinus = self.rateEquation(self.numMuMinus)
        self.rateMuPlus  = self.rateEquation(self.numMuPlus)
        self.rateMu = self.rateEquation(self.numMuMinus + self.numMuPlus)
        
    def rateEquation(self, numParticles):
        return numParticles * (self.POTperSpill / self.POTsimulated) / (self.area * 100.0**2) / self.spillDuration

    def UpdateParticleTypeShown(self, pTypeShown):
        if pTypeShown == 'muAll':
            self.rateSelected = self.rateMu
        elif pTypeShown == 'muMinus':
            self.rateSelected = self.rateMuMinus
        elif pTypeShown == 'muPlus':
            self.rateSelected = self.rateMuPlus
        else:
            print('Particle Type not found.')


class tilegrid():
    def __init__(self, data, zloc, xmin, xmax, ymin, ymax, tileSizeX, tileSizeY, partperjob, oversampling, POTperSpill, spillDuration, particleType='13'):
        self.data = data
        self.zloc = zloc
        self.particleType = particleType
        h = self.data.histograms2dpy["Event/SimpleHistograms/z{}mL_XY_+{}".format(str(int(self.zloc)), self.particleType)]
        self.zlocList = []
        for plotname in data.histograms2dpy.keys():
            if re.search(r'Event\/SimpleHistograms\/z(.*)mL_XY_\+13', plotname) is None:
                pass
            else:
                self.zlocList.append(int(re.search(r'Event\/SimpleHistograms\/z(.*)mL_XY_\+13', plotname).group(1)))
        self.minBinSizeX = np.abs(round(h.xedges[0]-h.xedges[1], 5))
        self.minBinSizeY = np.abs(round(h.yedges[0]-h.yedges[1], 5))
        self.xmin = myceil(xmin, self.minBinSizeX)
        self.xmax = myfloor(xmax, self.minBinSizeX)
        self.ymin = myceil(ymin, self.minBinSizeY)
        self.ymax = myfloor(ymax, self.minBinSizeY)
        self.tileSizeX = tileSizeX
        self.tileSizeY = tileSizeY
        self.partperjob = partperjob
        self.oversampling = oversampling
        self.POTperSpill = POTperSpill
        self.spillDuration = spillDuration
        self.pTypeShown = 'muAll'
        # self.gridMinX, self.gridMinY = np.meshgrid(h.xedges, h.yedges)
        self.gridMinX, self.gridMinY = np.meshgrid(h.xcentres, h.ycentres)
        self.xminPlot = h.xedges[0]
        self.xmaxPlot = h.xedges[-1]
        self.yminPlot = h.yedges[0]
        self.ymaxPlot = h.yedges[-1]
        self.wrtX = self.xmin
        self.wrtY = 0.0
        self.s = np.arange(1, int(myceil(self.gridMinX.shape[0] * 0.5)), 1)
        self.t = np.arange(1, int(myceil(self.gridMinX.shape[1] * 0.5)), 1)
        self.tileScaleX = np.ones_like(self.s)
        self.tileScaleY = np.ones_like(self.t)
        self.UpdateTileEdges(self.tileScaleX, self.tileScaleY)
        self.annotX = 0.0
        self.annotY = 0.0

    def UpdateTileEdges(self, tileScaleX, tileScaleY):
        self.tileScaleX = tileScaleX
        self.tileScaleY = tileScaleY
        self.tileEdgesX = [self.wrtX]
        for i in range(self.tileScaleX.size):
            self.tileEdgesX.append(self.tileEdgesX[i] + self.tileScaleX[i] * self.tileSizeX)
        hidden = 2 * self.wrtX - np.array(self.tileEdgesX[:][::-1])
        self.tileEdgesX = np.array(self.tileEdgesX)
        self.tileEdgesX = np.concatenate((hidden, self.tileEdgesX), axis=None)
        self.tileEdgesX = self.tileEdgesX[self.xmin <= self.tileEdgesX]
        self.tileEdgesX = self.tileEdgesX[self.tileEdgesX <= self.xmax]

        self.tileEdgesY = [self.wrtY]
        for i in range(self.tileScaleY.size):
            self.tileEdgesY.append(self.tileEdgesY[i] + self.tileScaleY[i] * self.tileSizeY)
        hidden = 2 * self.wrtY - np.array(self.tileEdgesY[:][::-1])
        self.tileEdgesY = np.array(self.tileEdgesY)
        self.tileEdgesY = np.concatenate((hidden, self.tileEdgesY), axis=None)
        self.tileEdgesY = self.tileEdgesY[self.ymin <= self.tileEdgesY]
        self.tileEdgesY = self.tileEdgesY[self.tileEdgesY <= self.ymax]

        self.tileCentresX = myround((self.tileEdgesX[1:] + self.tileEdgesX[:-1]) * 0.5, self.minBinSizeX)
        self.tileCentresY = myround((self.tileEdgesY[1:] + self.tileEdgesY[:-1]) * 0.5, self.minBinSizeY)
        self.tileNumX = len(self.tileCentresX)
        self.tileNumY = len(self.tileCentresY)
        self.tileNum = self.tileNumX * self.tileNumY
        self.tiles = [[tile(self.data, self.zloc, self.tileEdgesX[x], self.tileEdgesX[x+1], self.tileEdgesY[y], self.tileEdgesY[y+1], self.partperjob, self.oversampling, self.POTperSpill, self.spillDuration, pTypeShown=self.pTypeShown) for x in range(self.tileNumX)] for y in range(self.tileNumY)]
    
    def UpdateWRT(self, wrtX, wrtY):
        self.wrtX = myround(wrtX, self.minBinSizeX)
        self.wrtY = myround(wrtY, self.minBinSizeY)
        self.UpdateTileEdges(self.tileScaleX, self.tileScaleY)

    def UpdateZLocation(self, z_new):
        self.zloc = z_new
        self.tiles = [[tile(self.data, self.zloc, self.tileEdgesX[x], self.tileEdgesX[x+1], self.tileEdgesY[y], self.tileEdgesY[y+1], self.partperjob, self.oversampling, self.POTperSpill, self.spillDuration, pTypeShown=self.pTypeShown) for x in range(self.tileNumX)] for y in range(self.tileNumY)]
        
    def UpdatePOTperSpill(self, POTperSpill_new):
        self.POTperSpill = POTperSpill_new
        self.tiles = [[tile(self.data, self.zloc, self.tileEdgesX[x], self.tileEdgesX[x+1], self.tileEdgesY[y], self.tileEdgesY[y+1], self.partperjob, self.oversampling, self.POTperSpill, self.spillDuration, pTypeShown=self.pTypeShown) for x in range(self.tileNumX)] for y in range(self.tileNumY)]

    def UpdateSpillDuration(self, spillDuration_new):
        self.spillDuration = spillDuration_new
        self.tiles = [[tile(self.data, self.zloc, self.tileEdgesX[x], self.tileEdgesX[x+1], self.tileEdgesY[y], self.tileEdgesY[y+1], self.partperjob, self.oversampling, self.POTperSpill, self.spillDuration, pTypeShown=self.pTypeShown) for x in range(self.tileNumX)] for y in range(self.tileNumY)]

    def UpdateTileSize(self, tileSizeX_new, tileSizeY_new, tileScaleX_new, tileScaleY_new):
        self.tileSizeX = tileSizeX_new
        self.tileSizeY = tileSizeY_new
        self.tileScaleX = tileScaleX_new
        self.tileScaleY = tileScaleY_new
        self.UpdateTileEdges(self.tileScaleX, self.tileScaleY)        
                
    def UpdateSize(self, xmin_new, xmax_new, ymin_new, ymax_new):
        self.xmin = myceil(xmin_new, self.minBinSizeX)
        self.xmax = myfloor(xmax_new, self.minBinSizeX)
        self.ymin = myceil(ymin_new, self.minBinSizeY)
        self.ymax = myfloor(ymax_new, self.minBinSizeY)
        self.UpdateTileSize(self.tileSizeX, self.tileSizeY, self.tileScaleX, self.tileScaleY)

    def getTileRates(self):
        rateAndBoundaryList = np.array([[{'coordinates': tile.coordinates, 'rate': tile.rateSelected} for tile in self.tiles[i]] for i in range(len(self.tiles))]).flatten()
        Z = np.zeros_like(self.gridMinX)
        for i in range(self.gridMinX.shape[0]):
            x = np.round(self.gridMinX[0][i], 5)
            if self.xmin <= x <= self.xmax:
                for j in range(self.gridMinY.shape[1]):
                    y = np.round(self.gridMinY[j][0], 5)
                    if self.ymin <= y <= self.ymax:
                        for entry in rateAndBoundaryList:
                            tileregion = path.Path(entry['coordinates'])
                            if tileregion.contains_point([x, y]):
                                Z[j, i] = entry['rate']
        self.gridData = Z
        return Z
    
    def getTileRateAtXY(self, x, y):
        closestX = np.argmin(np.abs(self.gridMinX[0] - x))
        closestY = np.argmin(np.abs(self.gridMinY[:, 0] - y))
        self.annotX = x
        self.annotY = y
        return np.around(self.gridData[closestY, closestX], 2)

    def setTileRates(self, pTypeShown):
        self.pTypeShown = pTypeShown
        self.tiles = [[tile(self.data, self.zloc, self.tileEdgesX[x], self.tileEdgesX[x+1], self.tileEdgesY[y], self.tileEdgesY[y+1], self.partperjob, self.oversampling, self.POTperSpill, self.spillDuration, pTypeShown=pTypeShown) for x in range(self.tileNumX)] for y in range(self.tileNumY)]

def muonFluxPlot(rebdsimfilename, zloc=55, xmin=1.0, xmax=3.5, ymin=-1.0, ymax=1.5, tileSizeX=0.25, tileSizeY=0.25, partperjob=10000, oversampling=10, POTperSpill=3.8e12, spillDuration=4.5):
    data = pybdsim.Data.Load(rebdsimfilename)
    detector = tilegrid(data, zloc, xmin, xmax, ymin, ymax, tileSizeX, tileSizeY, partperjob, oversampling, POTperSpill, spillDuration)

    # add a way to make the tile regions elliptic (coordinates instead of edges) - add textbox f(x,y) to scale size with radius
    
    # create the figure
    fig, ax = plt.subplots(1, 1, figsize=(45, 7))
    plt.subplots_adjust(left=0.15, bottom=0.45)

    tfs = pymadx.Data.Tfs("K12.tfs")
    vmin = 1
    vmax = 100
    xInitMin = detector.xminPlot
    xInitMax = detector.xmaxPlot
    yInitMin = -2.0
    yInitMax = 2.0

    # plotTiles = ax.imshow(detector.getTileRates(), extent=(detector.tileEdgesX[0], detector.tileEdgesX[-1], detector.tileEdgesY[0], detector.tileEdgesY[-1]), cmap='viridis', norm=colors.LogNorm(vmin=vmin, vmax=vmax))
    plotTiles = ax.pcolormesh(detector.gridMinX, detector.gridMinY, detector.getTileRates(), cmap='viridis', norm=colors.LogNorm(vmin=vmin, vmax=vmax))
    plotDetector = ax.add_patch(patches.Rectangle( (detector.xmin, detector.ymin), np.abs(round(detector.xmax-detector.xmin)), np.abs(round(detector.ymax-detector.ymin)), edgecolor='r', facecolor=(0, 0, 0, 0), linewidth=0.5))
    zText = ax.text(0.007, 0.982, "Z = "+str(detector.zloc)+" m", transform=ax.transAxes, fontsize=10, verticalalignment='top', bbox=dict(fc=(1, 1, 1), ec=(0, 0, 0)))
    annot = ax.annotate("", xy=(0,0), xytext=(30,20), textcoords="offset points",
                        arrowprops=dict(arrowstyle='-|>'))
    annot.set_visible(False)
    ax.set_xlabel("x [cm]")
    ax.set_ylabel("y [cm]")
    ax.set_xlim([xInitMin, xInitMax])
    ax.set_ylim([yInitMin, yInitMax])

    
    fig.colorbar(plotTiles, ax=ax, label=r"Muon rate [1 / s / cm$^2$]", extend="max")

    axMachine = plt.axes([0.15, 0.90, 0.7, 0.08])
    pymadx.Plot._SetMachineAxesStyle(axMachine)
    pymadx.Plot._DrawMachineLattice(axMachine, tfs)

    machineMargin = np.abs(detector.zlocList[-1]-detector.zlocList[0])*0.01
    axMachine.set_xlim(detector.zlocList[0]-machineMargin, detector.zlocList[-1]+machineMargin)
    axMachine.set_ylim(-0.3, 0.3)
    for z in detector.zlocList:
        axMachine.axvline(z, color='lightgrey', lw=0.3, zorder=0)
    greenLine = axMachine.axvline(detector.zloc, color='lime', lw=0.6, zorder=100)

    axcolor = 'whitesmoke'
    ax_rangeX = plt.axes([0.15, 0.35, 0.55, 0.02], facecolor=axcolor)
    ax_rangeY = plt.axes([0.15, 0.32, 0.55, 0.02], facecolor=axcolor)
    ax_rangeZ = plt.axes([0.15, 0.29, 0.55, 0.02], facecolor=axcolor)
    ax_detectorRangeX = plt.axes([0.15, 0.25, 0.55, 0.02], facecolor=axcolor)
    ax_detectorRangeY = plt.axes([0.15, 0.22, 0.55, 0.02], facecolor=axcolor)
    ax_tileRangeX = plt.axes([0.15, 0.19, 0.55, 0.02], facecolor=axcolor)
    ax_tileRangeY = plt.axes([0.15, 0.16, 0.55, 0.02], facecolor=axcolor)
    ax_POTperSpill = plt.axes([0.15, 0.13, 0.55, 0.02], facecolor=axcolor)
    ax_spillDuration = plt.axes([0.15, 0.10, 0.55, 0.02], facecolor=axcolor)

    s_rangeX = RangeSlider(ax_rangeX, 'x [m]', detector.xminPlot, detector.xmaxPlot, valinit=(xInitMin, xInitMax), valstep=detector.minBinSizeX)
    s_rangeY = RangeSlider(ax_rangeY, 'y [m]', detector.yminPlot, detector.ymaxPlot, valinit=(yInitMin, yInitMax), valstep=detector.minBinSizeY)
    s_rangeZ = RangeSlider(ax_rangeZ, r'Muon Rate [1/s/cm$^2$]', vmin, 10*vmax, valinit=(vmin, vmax), valstep=1)
    s_detectorRangeX = RangeSlider(ax_detectorRangeX, 'Detector Location x [m]', detector.xminPlot, detector.xmaxPlot, valinit=(detector.xmin, detector.xmax), valstep=detector.minBinSizeX)
    s_detectorRangeY = RangeSlider(ax_detectorRangeY, 'Detector Location y [m]', detector.yminPlot, detector.ymaxPlot, valinit=(detector.ymin, detector.ymax), valstep=detector.minBinSizeY)
    s_tileSizeX = Slider(ax_tileRangeX, 'Tile Size x [m]', 2 * detector.minBinSizeX, 3.5, valinit=detector.tileSizeX, valstep=detector.minBinSizeX)
    s_tileSizeY = Slider(ax_tileRangeY, 'Tile Size y [m]', 2 * detector.minBinSizeY, 3.5, valinit=detector.tileSizeY, valstep=detector.minBinSizeY)
    s_POTperSpill = Slider(ax_POTperSpill, 'POT per Spill', 0.1e12, 10.0e12, valinit=detector.POTperSpill, valstep=1e11)
    s_spillDuration = Slider(ax_spillDuration, 'Spill Duration [s]', 0.1, 10, valinit=detector.spillDuration, valstep=0.1)
    
    def update(val):
        rangeX = s_rangeX.val
        rangeY = s_rangeY.val
        rangeZ = s_rangeZ.val
        detectorRangeX = s_detectorRangeX.val
        detectorRangeY = s_detectorRangeY.val
        tileSizeX = s_tileSizeX.val
        tileSizeY = s_tileSizeY.val
        POTperSpill = s_POTperSpill.val
        spillDuration = s_spillDuration.val

        ax.set_xlim(rangeX[0], rangeX[1])
        ax.set_ylim(rangeY[0], rangeY[1])
        ax.relim()
        ax.autoscale_view()
        # update detector data
        detector.UpdateSize(detectorRangeX[0], detectorRangeX[1], detectorRangeY[0], detectorRangeY[1])
        detector.UpdateTileSize(tileSizeX, tileSizeY, detector.tileScaleX, detector.tileScaleY)
        detector.UpdatePOTperSpill(POTperSpill)
        detector.UpdateSpillDuration(spillDuration)
        # update plot
        plotTiles.set_norm(colors.LogNorm(vmin=rangeZ[0], vmax=rangeZ[1]))
        plotTiles.set_array(detector.getTileRates().ravel())
        plotDetector.set_bounds(detector.xmin, detector.ymin, np.abs(round(detector.xmax-detector.xmin, 5)), np.abs(round(detector.ymax-detector.ymin, 5)))
        annot.set_text(r"{} / s / cm$^2$".format(detector.getTileRateAtXY(detector.annotX, detector.annotY)))
        # fig.canvas.draw_idle()
        plt.draw()
    s_rangeX.on_changed(update)
    s_rangeY.on_changed(update)
    s_rangeZ.on_changed(update)
    s_detectorRangeX.on_changed(update)
    s_detectorRangeY.on_changed(update)
    s_tileSizeX.on_changed(update)
    s_tileSizeY.on_changed(update)
    s_POTperSpill.on_changed(update)
    s_spillDuration.on_changed(update)

    def submitX(expression):
        if 'x' not in expression:
            detector.tileScaleX = np.ones_like(detector.s)
        else:
            detector.tileScaleX = eval(expression.replace('x', 'detector.s'))
        detector.UpdateTileEdges(detector.tileScaleX, detector.tileScaleY)
        plotTiles.set_array(detector.getTileRates().ravel())
        plotDetector.set_bounds(detector.xmin, detector.ymin, np.abs(round(detector.xmax-detector.xmin, 5)), np.abs(round(detector.ymax-detector.ymin, 5)))
        annot.set_text(r"{} / s / cm$^2$".format(detector.getTileRateAtXY(detector.annotX, detector.annotY)))
        plt.draw()
        # fig.canvas.draw_idle()
    axboxX = plt.axes([0.15, 0.04, 0.25, 0.04])
    textBoxX = TextBox(axboxX, "f(x) = ", textalignment="center")
    textBoxX.on_submit(submitX)
    textBoxX.set_val("1")

    def submitY(expression):
        if 'y' not in expression:
            detector.tileScaleY = np.ones_like(detector.t)
        else:
            detector.tileScaleY = eval(expression.replace('y', 'detector.t'))
        detector.UpdateTileEdges(detector.tileScaleX, detector.tileScaleY)
        plotTiles.set_array(detector.getTileRates().ravel())
        plotDetector.set_bounds(detector.xmin, detector.ymin, np.abs(round(detector.xmax-detector.xmin, 5)), np.abs(round(detector.ymax-detector.ymin, 5)))
        annot.set_text(r"{} / s / cm$^2$".format(detector.getTileRateAtXY(detector.annotX, detector.annotY)))
        plt.draw()
        # fig.canvas.draw_idle()
    axboxY = plt.axes([0.45, 0.04, 0.25, 0.04])
    textBoxY = TextBox(axboxY, "f(y) = ", textalignment="center")
    textBoxY.on_submit(submitY)
    textBoxY.set_val("1")

    # Defining the cursor
    cursor = Cursor(ax, horizOn=True, vertOn=True, useblit=True, color = 'k', linewidth = 0.3)
    wrtax = plt.axes([0.15, 0.015, 0.2, 0.03])
    wrtax.axis('off')
    wrtText = wrtax.text(0, 0, "scaled with respect to: (x, y) = ({}, {})".format(detector.wrtX, detector.wrtY))
    def onclickAx(event):
        if event.inaxes == ax:
            x = event.xdata
            y = event.ydata
            if event.dblclick:
                detector.UpdateWRT(x, y)
                wrtText.set_text("scaled with respect to: (x, y) = ({}, {})".format(detector.wrtX, detector.wrtY))
                plotTiles.set_array(detector.getTileRates().ravel())
                plotDetector.set_bounds(detector.xmin, detector.ymin, np.abs(round(detector.xmax-detector.xmin, 5)), np.abs(round(detector.ymax-detector.ymin, 5)))
                annot.set_text(r"{} / s / cm$^2$".format(detector.getTileRateAtXY(detector.annotX, detector.annotY)))
            else:
                if detector.xmin < x < detector.xmax and detector.ymin < y < detector.ymax:
                    annot.set_text(r"{} / s / cm$^2$".format(detector.getTileRateAtXY(x, y)))
                    annot.xy = (x, y)
                    annot.set_visible(True)
                else :
                    annot.set_visible(False)
            plt.draw() #redraw the figure
    fig.canvas.mpl_connect('button_press_event', onclickAx)

    # Defining the cursor
    machineCursor = Cursor(axMachine, horizOn=False, vertOn=True, useblit=True, color = 'k', linewidth = 0.3)
    def onclickMachine(event):
        if event.inaxes == axMachine:
            zNew = min(detector.zlocList, key=lambda x:abs(x-event.xdata)) 
            detector.UpdateZLocation(zNew)
            zText.set_text("z = {}m".format(int(detector.zloc)))
            greenLine.set_xdata([detector.zloc, detector.zloc])
            plotTiles.set_array(detector.getTileRates().ravel())
            plotDetector.set_bounds(detector.xmin, detector.ymin, np.abs(round(detector.xmax-detector.xmin, 5)), np.abs(round(detector.ymax-detector.ymin, 5)))
            annot.set_text(r"{} / s / cm$^2$".format(detector.getTileRateAtXY(detector.annotX, detector.annotY)))
            plt.draw() #redraw the figure
    fig.canvas.mpl_connect('button_press_event', onclickMachine)

    rax = plt.axes([0.8, 0.1, 0.07, 0.13], facecolor=axcolor)
    radio = RadioButtons(rax, ('$\mu^-+\mu^+$', '$\mu^-$', '$\mu^+$'), activecolor='k')
    def selectParticle(label):
        pDict = {'$\mu^-+\mu^+$': 'muAll', '$\mu^-$': 'muMinus', '$\mu^+$': 'muPlus'}
        # update detector rates according to selected particle
        detector.setTileRates(pDict[label])
        # update plot
        plotTiles.set_array(detector.getTileRates().ravel())
        plotDetector.set_bounds(detector.xmin, detector.ymin, np.abs(round(detector.xmax-detector.xmin, 5)), np.abs(round(detector.ymax-detector.ymin, 5)))
        annot.set_text(r"{} / s / cm$^2$".format(detector.getTileRateAtXY(detector.annotX, detector.annotY)))
        # fig.canvas.draw_idle()
        plt.draw()
    radio.on_clicked(selectParticle)

    resetax = plt.axes([0.8, 0.04, 0.07, 0.04])
    button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')
    def reset(event):
        s_rangeX.reset()
        s_rangeY.reset()
        s_rangeZ.reset()
        s_detectorRangeX.reset()
        s_detectorRangeY.reset()
        s_tileSizeX.reset()
        s_tileSizeY.reset()
        s_POTperSpill.reset()
        s_spillDuration.reset()
    button.on_clicked(reset)

    plt.show()


if __name__ == '__main__':

    rebdsimfilename = sys.argv[1]
    group = sys.argv[2]
    setup = int(sys.argv[3])

    # partperjob = int(sys.argv[2])
    # oversampling = int(sys.argv[3])
    
    # For setup_shadows.root
    partperjob = 10000
    oversampling = 10

    # SHADOWS FoM
    # muonFluxPlot(rebdsimfilename)
    # SHADOWS muon measurement
    # muonFluxPlot(rebdsimfilename, zloc=70, xmin=1.0, xmax=4.0, ymin=-1.0, ymax=1.0)
    # ANTI-0
    # muonFluxPlot(rebdsimfilename, zloc=105, xmin=-1.08, xmax=1.08, ymin=-1.08, ymax=1.08)
    # MUV3
    # muonFluxPlot(rebdsimfilename, zloc=247, xmin=-1.32, xmax=1.32, ymin=-1.32, ymax=1.32)

    if group == 'Mainz':
        # Mainz Group - Setups
        detsize = {'x': 0.19, 'y': 0.19, 'z': 0.03}
        pos = {'x': 3.12, 'y': -0.65, 'z': 70}       # only location    
        tilesize = {'x': 0.19, 'y': 0.19}
    elif group == 'Italy':
        # Italy Group - Setups
        detsize = {'x': 0.9, 'y': 0.6, 'z': 0.12}
        if setup == 1:
            pos = {'x': 2.91, 'y': -0.24, 'z': 71}       # first location
        elif setup == 2:
            pos = {'x': 3.21, 'y': -0.24, 'z': 71}       # second location
        elif setup == 3 or setup == 4:
            pos = {'x': 2.61, 'y': -0.24, 'z': 71}       # third and fourth location
        else:
            raise AssertionError ("Setup not found. Please choose between 1 and 4.")
        tilesize = {'x': 0.15, 'y': 0.15}
    elif group == 'Heidelberg':
        # Heidelberg Group - Setups
        detsize = {'x': 0.2, 'y': 0.2, 'z': 0.04}
        if setup == 1 or setup == 2:
            pos = {'x': 3.39, 'y': -0.23, 'z': 73}       # first and second location
        elif setup == 3:
            pos = {'x': 2.40, 'y': -0.23, 'z': 73}       # third location
        elif setup == 4:
            pos = {'x': 2.93, 'y': -0.23, 'z': 73}       # third location
        else:
            raise AssertionError ("Setup not found. Please choose between 1 and 4.")
        tilesize = {'x': 0.2, 'y': 0.2}
    else:
        raise AssertionError ("Group not found. Please choose between Mainz, Italy and Heidelberg.")
    muonFluxPlot(rebdsimfilename, zloc=pos['z'], xmin=myfloor(pos['x']-detsize['x']*0.5, 0.05), xmax=myceil(pos['x']+detsize['x']*0.5, 0.05), ymin=myfloor(pos['y']-detsize['y']*0.5, 0.05), ymax=myceil(pos['y']+detsize['y']*0.5, 0.05), tileSizeX=tilesize['x'], tileSizeY=tilesize['y'], partperjob=partperjob, oversampling=oversampling, POTperSpill=1.0e12, spillDuration=1.0)
