# shadows-muon-flux-simulation



## Name
SHADOWS muon flux simulation for the measurement taken between 19-28. June 2023.


## Description
Interactive python tool to visualize and investigate the simulated muon flux in the K12 beam line.


## Usage instructions
The tool can be used by just calling it via

ipython <path/to/rootfile.root> <group> <setupnr>

where
<path/to/rootfile.root> must be one of the root files provided in the same folder
<group> must be eiter mainz, heidlberg or italy 
<setupnr> defines the detector position given by one of the four setups that was used (see setups.pdf)
(the last two variables are just used to set the initial position of the detector in the interactive plot)


## Getting started with the interactive plot
The interactive plot allows the user to
- vary several parameters using sliders
- changing the z-location by clicking at the grey lines in the lattice diagram at the top
- click the plot to evaluate the muon rate at a given location
- double-click the plot to move the origin of the tile within the detector to another location
- use tilesizes that are adjustable by a given formula for x or y (e.g. f(x) = x ** 2, f(y) = 2 * y)

Note that you will need to double-click the plot to move the tile position within your detector. Also note that if you only see white space, the tile size might exceed your detector size (just make your detector slightly larger).

## Project status
The tool is available and ready to use. If you have ideas for additional features to add in the future, contact florian.wolfgang.stummer@cern.ch